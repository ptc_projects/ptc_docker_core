import os

ABS_PATH = os.path.abspath(os.sep)

# File paths
SOURCES_DIRECTORY_PATH = os.path.join(ABS_PATH, "src")
TEST_DATA_DIRECTORY_PATH = os.path.join(SOURCES_DIRECTORY_PATH, "test_data", os.environ.get("SUBMISSION_NAME"))
SUBMISSION_DIRECTORY_PATH = os.path.join(SOURCES_DIRECTORY_PATH, os.environ.get("SUBMISSION_NAME"))
CUSTOM_TEST_DATA_DIRECTORY_PATH = os.path.join(SUBMISSION_DIRECTORY_PATH, "custom_test_cases")
UNIT_TEST_DATA_DIRECTORY_PATH = os.path.join(SUBMISSION_DIRECTORY_PATH, "tests")
SUBMISSION_CONFIG_FILE_PATH = os.path.join(TEST_DATA_DIRECTORY_PATH, "config.json")

# GitLab info
GITLAB_ACCESS_TOKEN = os.environ.get("GITLAB_ACCESS_TOKEN")
PROJECT_ID = os.environ.get("PROJECT_ID")
MERGE_REQUEST_IID = os.environ.get("MERGE_REQUEST_IID")
GITLAB_USER_NAME = os.environ.get("GITLAB_USER_NAME")

# ToolExecutor settings
TOOL_TIME_OUT = 25  # in seconds

# IOTestsExecutor settings
IS_DIAGNOSTIC = os.environ.get("IS_DIAGNOSTIC", default="not_diagnostic") == "diagnostic"
TEST_NAME_LEN = 4  # "1.in": 1, "01.in": 2, "001.in": 3, etc.
IO_TESTS_TIME_OUT = 5  # in seconds

# MRDescLogger
DEADLINE_TIME_TEMPLATE = "%d-%m-%Y %H:%M %z"  # 30-01-2021 21:33 ±0400

# Fake environment variables for subprocesses (running students' code)
FAKE_ENV = {i: "" for i in (
    "IS_DIAGNOSTIC",
    "SUBMISSION_NAME",
    "GITLAB_ACCESS_TOKEN",
    "PROJECT_ID",
    "MERGE_REQUEST_IID",
    "GITLAB_USER_NAME",
    "PIPELINE_CONFIG_FILENAME"
)}
