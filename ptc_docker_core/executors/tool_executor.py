import os
import subprocess
from subprocess import PIPE, STDOUT
from typing import Optional, List, Tuple

from ptc_docker_core.constants import SUBMISSION_DIRECTORY_PATH, TOOL_TIME_OUT, FAKE_ENV
from ptc_docker_core.executors.abstract_executor import AbstractExecutor
from ptc_docker_core.loggers.abstract_logger import AbstractLogger
from ptc_docker_core.helper_classes import TaskStatus, State, TaskExecutionInfo


class ToolExecutor(AbstractExecutor):
    """
    This class is used for running some tools for tasks (e.g. linters)
    """
    def __init__(
            self,
            state_name: str,
            run_cmd: str,
            file_extension: str,
            execution_directory_path: str = SUBMISSION_DIRECTORY_PATH,
            loggers: Optional[List[AbstractLogger]] = None
    ):
        super().__init__(state_name, loggers)
        self.execution_directory_path = execution_directory_path
        self.run_cmd = run_cmd
        self.file_extension = file_extension

    def _run_subprocess(self, task_name: str) -> Tuple[TaskStatus, str, int]:
        """
        This method runs tool to the given task and gets information about execution

        :param task_name: name of checked task
        :return: tuple with information about the checked task (TaskStatus, logs, return code)
        """
        task_path = os.path.join(self.execution_directory_path, f"{task_name}{self.file_extension}")
        if not os.path.isfile(task_path):
            return TaskStatus.NOT_SUBMITTED, "", 1

        process = subprocess.run(
            f"{self.run_cmd} {task_path}",
            shell=True,
            stdout=PIPE,
            stderr=STDOUT,
            timeout=TOOL_TIME_OUT,
            env={**os.environ, **FAKE_ENV}
        )

        return (
            TaskStatus.FAILED if process.returncode else TaskStatus.PASSED,
            process.stdout.decode("utf-8") if process.returncode else "",
            process.returncode
        )

    def _run(self, task_names: List[str]) -> State:
        """
        This method runs tool for every task in given list and gets information about execution

        :param task_names: list of tasks names
        :return: a State dataсlass instance which contains all information about some testing pipeline stage
        """
        tasks_execution_info = []
        is_passed = True

        for task_name in task_names:
            status, logs, exit_code = self._run_subprocess(task_name)
            is_passed &= exit_code == 0
            tasks_execution_info.append(TaskExecutionInfo(task_name, status, "", logs))

        return State(self.state_name, is_passed, tasks_execution_info)
