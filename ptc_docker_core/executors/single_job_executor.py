import subprocess
from subprocess import PIPE
from typing import Optional, List

from ptc_docker_core.constants import TOOL_TIME_OUT
from ptc_docker_core.executors.abstract_executor import AbstractExecutor
from ptc_docker_core.loggers.abstract_logger import AbstractLogger
from ptc_docker_core.helper_classes import State


class SingleJobExecutor(AbstractExecutor):
    """
    This class is used to run simple command (e.g. mkdir, cp)
    """
    def __init__(self, state_name: str, run_cmd: str, loggers: Optional[List[AbstractLogger]] = None) -> None:
        super().__init__(state_name, loggers)
        self.run_cmd = run_cmd

    def execute(self, task_names: List[str]) -> State:
        """
        This method calls _run method

        :param task_names: not used in current implementation
        :return: always `State("", True, [])`
        """
        return self._run(task_names)

    def _run(self, task_names: List[str]) -> State:
        """
        This method runs command and always returns empty success State dataclass instance about it's execution

        :param task_names: not used in current implementation
        :return: always `State("", True, [])`
        """
        subprocess.run(
            f"{self.run_cmd}",
            shell=True,
            stdout=PIPE,
            stderr=PIPE,
            timeout=TOOL_TIME_OUT
        )

        return State("", True, [])
