import abc
from typing import Optional, List

from ptc_docker_core.loggers.abstract_logger import AbstractLogger
from ptc_docker_core.helper_classes import State, ClassesByNamesRegistry


class AbstractExecutor(abc.ABC, ClassesByNamesRegistry):
    """
    This class provides interface and basic functionality for all executors
    """
    def __init__(self, state_name: str, loggers: Optional[List[AbstractLogger]] = None) -> None:
        self.state_name = state_name
        self.loggers = [] if loggers is None else loggers

    @abc.abstractmethod
    def _run(self, task_names: List[str]) -> State:
        """
        Abstract method. Implementations should run special execution

        :param task_names: list of tasks names
        :return: must return a dataсlass instance which contains all information about some testing pipeline stage
        """
        pass

    def __log(self, state: State) -> None:
        """
        This method adds executor's state to each logger stored in `self.loggers` list

        :param state: a dataсlass instance which contains all information about some testing pipeline stage
        :return: None
        """
        for logger in self.loggers:
            logger.add_state(state)

    def execute(self, task_names: List[str]) -> State:
        """
        This method runs executor and logs returned state

        :param task_names: list of tasks names from config.json
        :return State: a dataсlass instance which contains all information about some testing pipeline stage
        """
        state = self._run(task_names)
        self.__log(state)
        return state
